import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import List from './List';
import Form from './Form';

const App = () => {
  const [orders, setOrders] = useState([]);
  const [selectedOrder, setSelectedOrder] = useState(null);
  const handleAddOrder = ({ name, price, notes = '' }) => {
    setOrders([...orders, { name, price, notes }]);
  };
  useEffect(() => {console.log('abhi', orders)}, [orders]);
  const handleUpdateOrder = (index, { name, price, notes = '' }) => {
    const stateOrders = orders;
    if (stateOrders[index]) {
      stateOrders[index] = {
        name,
        price,
        notes
      }
      setOrders([...stateOrders]);
    }
  };
  const renderAdd = () => {
    return (
      <Form
        handleAddOrder={handleAddOrder}
      />
    );
  };
  const renderUpdate = (props) => {
    const { params } = props.match || {};
    console.log('abhi', params.id);
    // setSelectedOrder(params.id);
    return (
      <Form
        selectedOrder={params.id}
        order={orders[params.id] || {}}
        handleAddOrder={handleAddOrder}
        handleUpdateOrder={handleUpdateOrder}
      />
    );
  };
  const onDelete = (id) => {
    const newOrders = orders.filter((i, index) => index !== id);
    setOrders(newOrders);
  };
  return (
    <Router>
      <div className="container">
        <header className="header">
          <div className="headerContent">
            <h3 className="logo">Daily Drinks</h3>
            <nav>
              <Link className="menuItem" to="/orders">
                List
              </Link>
              <Link className="menuItem" to="/add">
                Add
              </Link>
            </nav>
          </div>
        </header>
        <main className="pageContainer">
          <Switch>
            <Route path="/" exact component={renderAdd} />
            <Route path="/orders" exact render={() => <List orders={orders} onDelete={onDelete} />} />
            <Route path="/add" exact component={renderAdd} />
            <Route path="/update/:id" exact render={renderUpdate} />
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
