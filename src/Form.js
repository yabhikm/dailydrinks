import React, { useState } from 'react';

const Form = ({
    selectedOrder,
    order,
    handleAddOrder,
    handleUpdateOrder,
}) => {
    const [formData, setFormData] = useState(selectedOrder ? order : {});
    const onSubmit = () => {
        if (!formData.name || !formData.price) return;
        if (selectedOrder) {
            handleUpdateOrder(selectedOrder, formData);
        } else {
            handleAddOrder(formData);
        }
    };
    const onChange = (e) => {
        const newFormData = {...formData};
        newFormData[e.target.name] = e.target.value;
        setFormData(newFormData);
    }
    return (
        <>
            <div className="pageHeading">
            <h3>{ selectedOrder ? 'Update Order' : 'Add Order' }</h3>
            </div>
            <div className="formContainer">
                <form class="jss3" novalidate="">
                    <div className="formWrapper">
                    <div className="fieldHalf">
                        <input type="text" value={formData.name || ''} className="inputText" placeholder="Name" name="name" onChange={onChange} />
                    </div>
                    <div className="fieldHalf">
                        <input className="inputText" min="0" value={formData.price || 0} type="number" placeholder="Price" name="price" onChange={onChange} />
                    </div>
                    <div className="fieldFull">
                        <textarea className="textArea" name="notes" placeholder="Notes" value={formData.notes || ''} onChange={onChange}></textarea>
                    </div>
                    <div className="fieldFull">
                        <button type="button" name="submit" className="button" onClick={onSubmit}>{selectedOrder ? 'Update order' : 'Place Order'}</button>
                    </div>
                    </div>
                </form>
            </div>
        </>
    );
};

export default Form;