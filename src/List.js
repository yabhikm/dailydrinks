import React from 'react';
import { Link } from "react-router-dom";

const List = ({
    orders = [],
    onDelete,
}) => {
    return (
        <>
            <div className="pageHeading">
            <h3>Orders</h3>
            </div>
            <div className="listContainer">
                <div className="listRow listHeader">
                    <div className="listCell">
                        Name
                    </div>
                    <div className="listCell">
                        Price
                    </div>
                    <div className="listCell">
                        Notes
                    </div>
                    <div className="listCell">
                        Action
                    </div>
                </div>
                {
                    orders.length ? (                        
                        orders.map((i, index) => (
                            <div className="listRow">
                                <div className="listCell">
                                    {i.name}
                                </div>
                                <div className="listCell">
                                    {i.price}
                                </div>
                                <div className="listCell">
                                    {i.notes}
                                </div>
                                <div className="listCell">
                                    <Link to={`/update/${index}`}>Edit</Link> | <a href="/#" onClick={(e) => {e.preventDefault();onDelete(index)}}>Delete</a>
                                </div>
                            </div>
                        ))
                    ) : (
                        <div className="listRow">
                            No orders
                        </div>
                    )
                }
            </div>
        </>
    );
};

export default List;